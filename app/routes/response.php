<?php
//Funcion para error 404//
function noexiste($accept){
if($accept =='aplication/json'){	
	 $mensaje='{"mensaje":"El dato solicitado no existe"}';
	return $mensaje;
	}
else{
	$mensaje='<?xml version="1.0" encoding="UTF-8"?><mensaje>El dato solicitado no existe</mensaje>';
	return $mensaje;
	}
}
//VERIFICA LA cve de 2 digitos
function value_cve_2($cve) {
  if (strlen($cve) != 2) {
    return true;
  }
return false;
}
//VERIFICA LA cve de 3 digitos
function value_cve_3($cve) {
  if (strlen($cve) != 3) {
    return true;
  }
return false;
}
//VERIFICA LA cve de 4 digitos
function value_anio($cve) {
  if (strlen($cve) != 4) {
    return true;
  }
return false;
}
//VERIFICA LA cve de 10 digitos
function value_cve_10($cve) {
  if (strlen($cve) != 10) {
    return true;
  }
return false;
}
//----------------------------------------------------------------------------GETS-------------------------------------------------------------------//
//-------TODAS LAS ENTIDADES -------------------------------------------------//
$app->get('/entidades/', function() use($app) {
$accept = $app->request->headers->get('accept');
  $datos = array('entidades' => array( ) );
  $entidades = $app->db->query("select * from entidad");
 if ($entidades->rowCount() == 0) {
	$app->response->headers->set('Content-Type', $accept);
	$app->halt(404, noexiste($accept));
	}
    foreach ($entidades as $entidad) {
    $datos['entidades'][] = $entidad;
  }
if($accept =='aplication/json'){
 $app->response->headers->set('Content-Type', 'application/json');  
 $app->response->setStatus(200);	
 $app->render('entidades.json.php', $datos);
}
else{
  $app->response->headers->set('Content-Type', 'application/xml');  
  $app->response->setStatus(200);	
  $app->render('entidades.xml.php', $datos);
 }
});

//----------UNA ENTIDAD EN ESPECIFICO-------------------------------------- //
$app->get('/entidades/:cve_entidad/', function($cve_entidad) use($app) {
$accep = $app->request->headers->get('Accept');
 if (value_cve_2($cve_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_entidad no cumple con el formato valido"}');	  
	}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_entidad no cumple con el formato valido</mensaje>');
	}
  }
  $datos = array('entidades' => array( ) );
  $entidades = $app->db->query("select * from entidad where cve_entidad='$cve_entidad'");
if ($entidades->rowCount() == 0) {
	$app->response->headers->set('Content-Type', $accep);
	$app->halt(404, noexiste($accep));
	}
foreach ($entidades as $entidad) {
  $datos['entidades'][] = $entidad;
  }
if($accep=='aplication/json'){
  $app->response->headers->set('Content-Type', 'application/json');  
  $app->response->setStatus(200);	
  $app->render('entidad.json.php', $datos);
}
else{
  $app->response->headers->set('Content-Type', 'application/xml');  
  $app->response->setStatus(200);	
  $app->render('entidad.xml.php', $datos);
}
});
//--------UNA ENTIDAD EN ESPECIFICO Y TODOS SU MUNICIPIOS --------------------------------//
$app->get('/entidades/:cve_entidad/municipios/', function($cve_entidad) use($app) {
$accep = $app->request->headers->get('Accept');
 if (value_cve_2($cve_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_entidad no cumple con el formato valido"}');	  
}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400,'<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_entidad no cumple con el formato valido</mensaje>');
	}
  }
  $datos = array('tmunicipios' => array( ) );
  $tmunicipios = $app->db->query("select * from municipio where cve_entidad='$cve_entidad'");
if ($tmunicipios->rowCount() == 0) {
	$app->response->headers->set('Content-Type', $accep);
	$app->halt(404, noexiste($accep));
  }
foreach ($tmunicipios as $tmunicipio) {
   $datos['tmunicipios'][] = $tmunicipio;
  }
if($accep=='aplication/json'){
 $app->response->headers->set('Content-Type', 'application/json');  
 $app->response->setStatus(200);	
 $app->render('municipios.json.php', $datos);
} 
else{
 $app->response->headers->set('Content-Type', 'application/xml');  
 $app->response->setStatus(200);	
 $app->render('municipios.xml.php', $datos);
	}
});
//-------------------------UNA ENTIDAD EN ESPECIFICO Y UN MUNICIPIO EN ESPECIFICO-------------------//
$app->get('/entidades/:cve_entidad/municipios/:cve_municipio/', function($cve_entidad,$cve_municipio) use($app) {
$accep = $app->request->headers->get('Accept');
 if (value_cve_2($cve_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_entidad no cumple con el formato valido"}');	  
}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400,'<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_entidad no cumple con el formato valido</mensaje>');
	}
  }
 if (value_cve_3($cve_municipio)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_municipio no cumple con el formato valido"}');	  
	}
   else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_municipio no cumple con el formato valido</mensaje>');
	}
  }
 
  $datos = array('tmunicipios' => array( ) );
  $tmunicipios = $app->db->query("select * from municipio where cve_entidad='$cve_entidad' and cve_municipio='$cve_municipio'");
if ($tmunicipios->rowCount() == 0) {
    $app->response->headers->set('Content-Type', $accep);
	$app->halt(404, noexiste($accep));
  }
  foreach ($tmunicipios as $tmunicipio) {
    $datos['tmunicipios'][] = $tmunicipio;
  }
if($accep=='aplication/json'){
 $app->response->headers->set('Content-Type', 'application/json');  
 $app->response->setStatus(200);	
 $app->render('municipio.json.php', $datos);
} 
  else{
 $app->response->headers->set('Content-Type', 'application/xml');  
  $app->response->setStatus(200);	
  $app->render('municipio.xml.php', $datos);
}
});

//---------------------TODOS LOS TEMAS DE UN MUNICIPIO Y ENTIDAD EN ESPECIFICO--------------------------------------//
$app->get('/entidades/:cve_entidad/municipios/:cve_municipio/temas/', function($cve_entidad,$cve_municipio) use($app) {
$accep = $app->request->headers->get('Accept');
 if (value_cve_2($cve_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_entidad no cumple con el formato valido"}');	  
}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400,'<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_entidad no cumple con el formato valido</mensaje>');
	}
  }
 if (value_cve_3($cve_municipio)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_municipio no cumple con el formato valido"}');	  
	}
   else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_municipio no cumple con el formato valido</mensaje>');
	}
  }
  $datos = array('ttemas' => array( ) );
  $ttemas = $app->db->query(
"select primertema.id_tema_1,primertema.tema_1,segundotema.id_tema_2,segundotema.tema_2,finanzas.id_tema_3,tercertema.tema_3
from finanzas
inner join tercertema on finanzas.id_tema_3=tercertema.id_tema_3 
inner join segundotema on tercertema.id_tema_2=segundotema.id_tema_2
inner join primertema on primertema.id_tema_1=segundotema.id_tema_1
where finanzas.cve_entidad='$cve_entidad' and finanzas.cve_municipio='$cve_municipio'
group by primertema.id_tema_1,primertema.tema_1,segundotema.id_tema_2,segundotema.tema_2,finanzas.id_tema_3,tercertema.tema_3 ");
if ($ttemas->rowCount() == 0) {
    $app->response->headers->set('Content-Type', $accep);
	$app->halt(404, noexiste($accep));
  }
  foreach ($ttemas as $ttema) {
    $datos['ttemas'][] = $ttema;
  }
if($accep=='aplication/json'){
 $app->response->headers->set('Content-Type', 'application/json');  
 $app->response->setStatus(200);	
 $app->render('temas.json.php', $datos);
} 
 else{
 $app->response->headers->set('Content-Type', 'application/xml');  
  $app->response->setStatus(200);	
  $app->render('temas.xml.php', $datos);
}
});
//----------------------UNA ENTIDAD Y UN MUNICIPIO EN ESPECIFICO CON UN TEMA EN ESPECIFICO------------------------------------------------//
$app->get('/entidades/:cve_entidad/municipios/:cve_municipio/temas/:cve_tema/', function($cve_entidad,$cve_municipio,$id_tema_3) use($app) {
$accep = $app->request->headers->get('Accept');
 if (value_cve_2($cve_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_entidad no cumple con el formato valido"}');	  
}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400,'<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_entidad no cumple con el formato valido</mensaje>');
	}
  }
 if (value_cve_3($cve_municipio)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_municipio no cumple con el formato valido"}');	  
	}
   else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_municipio no cumple con el formato valido</mensaje>');
	}
  
  }
 if (value_cve_2($id_tema_3)) {
  if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_tema no cumple con el formato valido"}');	  
	}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_tema no cumple con el formato valido</mensaje>');
	}
	
  }

  $datos = array('ttemas' => array( ) );
  $ttemas = $app->db->query("select
primertema.id_tema_1, primertema.tema_1, segundotema.id_tema_2, segundotema.tema_2, tercertema.id_tema_3,tercertema.tema_3
from tercertema
inner join segundotema on segundotema.id_tema_2=tercertema.id_tema_2
inner join primertema on primertema.id_tema_1=segundotema.id_tema_1 where tercertema.id_tema_3='$id_tema_3'
group by primertema.id_tema_1, primertema.tema_1, segundotema.id_tema_2, segundotema.tema_2, tercertema.id_tema_3,tercertema.tema_3");

if ($ttemas->rowCount() == 0) {
	  $app->response->headers->set('Content-Type', $accep);
	$app->halt(404, noexiste($accep));
  }
  foreach ($ttemas as $ttema) {
    $datos['ttemas'][] = $ttema;
  }
if($accep=='aplication/json'){
 $app->response->headers->set('Content-Type', 'application/json');  
 $app->response->setStatus(200);	
 $app->render('tema.json.php', $datos);
}
 else{
 $app->response->headers->set('Content-Type', 'application/xml');  
  $app->response->setStatus(200);	
  $app->render('tema.xml.php', $datos);
}
});

//---------------------TODOS LOS INDICADORES DE UN TEMA,UN MUNICIPIO Y UNA ENTIDAD EN ESPECIFICO---------------------//
$app->get('/entidades/:cve_entidad/municipios/:cve_municipio/temas/:cve_tema/indicadores/', function($cve_entidad,$cve_municipio,$id_tema_3) use($app) {
$accep = $app->request->headers->get('Accept');
 if (value_cve_2($cve_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_entidad no cumple con el formato valido"}');	  
}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400,'<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_entidad no cumple con el formato valido</mensaje>');
	}
  }
 if (value_cve_3($cve_municipio)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_municipio no cumple con el formato valido"}');	  
	}
   else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_municipio no cumple con el formato valido</mensaje>');
	}
  
  }
 if (value_cve_2($id_tema_3)) {
  if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_tema no cumple con el formato valido"}');	  
	}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_tema no cumple con el formato valido</mensaje>');
	}
	
  }

  $datos = array('tindicadores' => array( ) );
  $tindicadores = $app->db->query("select finanzas.id_indicador,indicador.indicador 
from finanzas
inner join indicador on finanzas.id_indicador=indicador.id_indicador 
where finanzas.cve_entidad='$cve_entidad' and finanzas.cve_municipio='$cve_municipio' and finanzas.id_tema_3='$id_tema_3'
group by finanzas.id_indicador,indicador.indicador order by indicador.indicador ASC");
if ($tindicadores->rowCount() == 0) {
      $app->response->headers->set('Content-Type', $accep);
	$app->halt(404, noexiste($accep));
  }
  foreach ($tindicadores as $tindicador) {
    $datos['tindicadores'][] = $tindicador;
  }
if($accep=='aplication/json'){
 $app->response->headers->set('Content-Type', 'application/json');  
 $app->response->setStatus(200);	
 $app->render('indicadores.json.php', $datos);
}
else{
 $app->response->headers->set('Content-Type', 'application/xml');  
  $app->response->setStatus(200);	
  $app->render('indicadores.xml.php', $datos);
}
});
//------------UN INDICADOR EN ESPECIFICO DE UN TEMA,UN MUNICIPIO Y UNA ENTIDAD EN ESPECIFICO-------------------//
$app->get('/entidades/:cve_entidad/municipios/:cve_municipio/temas/:cve_tema/indicadores/:cve_indicador', function($cve_entidad,$cve_municipio,$id_tema_3,$id_indicador) use($app) {
$accep = $app->request->headers->get('Accept');
 if (value_cve_2($cve_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_entidad no cumple con el formato valido"}');	  
}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400,'<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_entidad no cumple con el formato valido</mensaje>');
	}
  }
 if (value_cve_3($cve_municipio)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_municipio no cumple con el formato valido"}');	  
	}
   else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_municipio no cumple con el formato valido</mensaje>');
	}
  
  }
 if (value_cve_2($id_tema_3)) {
  if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_tema no cumple con el formato valido"}');	  
	}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_tema no cumple con el formato valido</mensaje>');
	}
	
  }
if (value_cve_10($id_indicador)) {
  if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_indicador no cumple con el formato valido"}');	  
	}
  else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_indicador no cumple con el formato valido</mensaje>');
	}
	
  }

$verifica_datos = $app->db->query("select * from municipio where cve_municipio='$cve_municipio' and cve_entidad='$cve_entidad'");
if ($verifica_datos->rowCount() == 0 ) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(404,'{"mensaje":"La entidad o municipio no existe"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(404, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La entidad o municipio no existe</mensaje>');
	}
  }
$verifica_tema = $app->db->query("select * from tercertema where id_tema_3='$id_tema_3'");
if ($verifica_tema->rowCount() == 0 ) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(404,'{"mensaje":"El tema no existe"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(404, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El tema no existe</mensaje>');
	}
  }

  $datos = array('tindicadores' => array( ) );
  $tindicadores = $app->db->query(
"select finanzas.id_indicador,indicador.indicador,anio.anio,finanzas.valor  from finanzas
inner join indicador on finanzas.id_indicador=indicador.id_indicador 
inner join anio on finanzas.id_anio=anio.id_anio
where finanzas.cve_entidad='$cve_entidad' and finanzas.cve_municipio='$cve_municipio' and finanzas.id_tema_3='$id_tema_3' 
and finanzas.id_indicador='$id_indicador'
group by finanzas.id_indicador,indicador.indicador,finanzas.id_anio,anio.anio,finanzas.valor 
order by anio.anio");
if ($tindicadores->rowCount() == 0) {
 $tindicadores = $app->db->query("select * from indicador where id_indicador='$id_indicador'");
    if ($tindicadores->rowCount() == 0) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(404,'{"mensaje":"El indicador no existe"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(404, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El indicador no existe</mensaje>');
	}
	}
  }

  foreach ($tindicadores as $tindicador) {
    $datos['tindicadores'][] = $tindicador;
  }
if($accep=='aplication/json'){
 $app->response->headers->set('Content-Type', 'application/json');  
 $app->response->setStatus(200);	
 $app->render('indicador.json.php', $datos);
} 
 else{
 $app->response->headers->set('Content-Type', 'application/xml');  
  $app->response->setStatus(200);	
  $app->render('indicador.xml.php', $datos);
}

});
//--------------------VALOR DE UN AÑO EN ESPECIFICO---------------------------------//
$app->get('/entidades/:cve_entidad/municipios/:cve_municipio/temas/:cve_tema/indicadores/:cve_indicador/', 
function($cve_entidad,$cve_municipio,$id_tema_3,$id_indicador) use($app) {
$accep = $app->request->headers->get('Accept');
$periodo = $app->request->params('periodo');
 if (value_cve_2($cve_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La cve_entidad no cumple con el formato valido"}');	  
}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400,'<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_entidad no cumple con el formato valido</mensaje>');
	}
  }
 if (value_cve_3($cve_municipio)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La cve_municipio no cumple con el formato valido"}');	  
	}
   else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_municipio no cumple con el formato valido</mensaje>');
	}
  
  }
 if (value_cve_2($id_tema_3)) {
  if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La cve_tema no cumple con el formato valido"}');	  
	}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_tema no cumple con el formato valido</mensaje>');
	}
	
  }
if (value_cve_10($id_indicador)) {
  if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La cve_indicador no cumple con el formato valido"}');	  
	}
  else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_indicador no cumple con el formato valido</mensaje>');
	}
	
  }
if (value_anio($periodo)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"El periodo no cumple con el formato valido"}');	  
	}
 	 else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El periodo no cumple con el formato valido</mensaje>');
	}

  }
  $datos = array('tindicadores' => array( ) );
  $tindicadores = $app->db->query(
"select finanzas.id_indicador,indicador.indicador,anio.anio,finanzas.valor  from finanzas
inner join indicador on finanzas.id_indicador=indicador.id_indicador 
inner join anio on finanzas.id_anio=anio.id_anio
where finanzas.cve_entidad='$cve_entidad' and finanzas.cve_municipio='$cve_municipio' and finanzas.id_tema_3='$id_tema_3' 
and finanzas.id_indicador='$id_indicador' and anio.anio='$periodo'
group by finanzas.id_indicador,indicador.indicador,finanzas.id_anio,anio.anio,finanzas.valor 
order by anio.anio");
if ($tindicadores->rowCount() == 0) {
    $app->response->headers->set('Content-Type', $accep);
	$app->halt(404, noexiste($accep));
  }
  foreach ($tindicadores as $tindicador) {
    $datos['tindicadores'][] = $tindicador;
  }
if($accep=='aplication/json'){
 $app->response->headers->set('Content-Type', 'application/json');  
 $app->response->setStatus(200);	
 $app->render('anio.json.php', $datos);
}
  else{
 $app->response->headers->set('Content-Type', 'application/xml');  
  $app->response->setStatus(200);	
  $app->render('anio.xml.php', $datos);
}
 
});


//--------------------------------------------------------------------------POSTS-------------------------------------------------//

//----------------POST ENTIDAD --------//

$app->post ('/entidades', function() use ($app){
$accep = $app->request->headers->get('Accept');
$g_body = $app->request->getBody();

if($accep ==='aplication/json'){
$body_json=json_decode($g_body);
if (!isset($body_json->entidad->desc_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"El formato JSON es incorecto"}');	  
	}
  }
$nueva_entidad = $body_json->entidad->desc_entidad;
//$app->halt(200, $nueva_entidad);
}
else{
$body_xml = new DOMDocument('1.0','UTF-8');
$body_xml->loadXML($g_body);
$nodos_entidad=$body_xml->getElementsByTagName('desc_entidad');
if ($nodos_entidad->length === 0 ) {
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El formato XML es incorecto</mensaje>');
	}
if($nodos_entidad->length ===1){
	$nodo_entidad =$nodos_entidad->item(0);
	$nueva_entidad =$nodo_entidad->nodeValue;
}
//$app->halt(200, $nueva_entidad);
}

$verifica = $app->db->query("select * from entidad where desc_entidad='$nueva_entidad'");
if ($verifica->rowCount() != 0 ) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(404,'{"mensaje":"La entidad no se puede duplicar"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(404, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La entidad no se puede duplicar</mensaje>');
	}
	
  }
$ultimo_id= $app->db->query("select cve_entidad from entidad order by cve_entidad desc limit 1");
foreach ($ultimo_id as $cve) {
    $id_entidad_ultimo=$cve['cve_entidad']+1;
  }
if($id_entidad_ultimo < 10){	
		$cve_entidad="0".$id_entidad_ultimo;
		}



//$app->halt(200,$cve_entidad);
$nuevo_registro= $app->db->query("insert into entidad values ('$cve_entidad','$nueva_entidad')");
 if ($nuevo_registro->rowCount() == 0) {
    	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(500,'{"mensaje":"El registro no se pudo agregar"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(500, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El registro no se pudo agregar</mensaje>');
	}
  }
 	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(201,'{"mensaje":"La entidad se agrego correctamente con id='.$cve_entidad.'"}' );	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(201, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La entidad se agrego correctamente con cve_entidad='.$cve_entidad.'</mensaje>');
	}

});
//--------------  POST MUNICIPIO ----------------------------//
$app->post ('/entidades/:cve_entidad/municipios', function($cve_entidad) use ($app){
$accep = $app->request->headers->get('Accept');
$g_body = $app->request->getBody();
if (value_cve_2($cve_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La cve_entidad no cumple con el formato valido"}');	  
}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400,'<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_entidad no cumple con el formato valido</mensaje>');
	}
  }
if($accep ==='aplication/json'){
$body_json=json_decode($g_body);
if (!isset($body_json->municipio->desc_municipio)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"El formato JSON es incorecto"}');	  
	}
  }
$nuevo_municipio = $body_json->municipio->desc_municipio;
}
else{
$body_xml = new DOMDocument('1.0','UTF-8');
$body_xml->loadXML($g_body);
$nodos_municipio=$body_xml->getElementsByTagName('desc_municipio');
if ($nodos_municipio->length === 0 ) {
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El formato XML es incorecto</mensaje>');
	}
if($nodos_municipio->length ===1){
	$nodo_municipio =$nodos_municipio->item(0);
	$nuevo_municipio =$nodo_municipio->nodeValue;
}
}
$verifica_entidad = $app->db->query("select * from entidad where cve_entidad='$cve_entidad'");
if ($verifica_entidad->rowCount() == 0 ) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(404,'{"mensaje":"La entidad a modificar no existe"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(404, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La entidad  no existe</mensaje>');
	}
  }
$verifica_municipio = $app->db->query("select * from municipio where cve_entidad='$cve_entidad' and desc_municipio='$nuevo_municipio'");
if ($verifica_municipio->rowCount() != 0 ) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"El municipio no se puede duplicar"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El municipio no se puede duplicar</mensaje>');
	}
  }
$ultimo_id= $app->db->query("select cve_municipio from municipio where cve_entidad='$cve_entidad' order by cve_municipio desc limit 1");

if($ultimo_id->rowCount() == 0 ){
$cve_municipio="000";
}
else{
foreach ($ultimo_id as $cve) {
    $id_municipio_ultimo=$cve['cve_municipio']+1;
  }
	if($id_municipio_ultimo < 100){	
		if($id_municipio_ultimo < 10){			
			$cve_municipio="00".$id_municipio_ultimo;
		}
		else{			
			$cve_municipio="0".$id_municipio_ultimo;
		}
	}
}
$nuevo_registro= $app->db->query("insert into municipio values ('$cve_municipio','$cve_entidad','$nuevo_municipio')");
 if ($nuevo_registro->rowCount() === 0) {
    	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(500,'{"mensaje":"El municipio no se pudo agregar"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(500, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El municipio no se pudo agregar</mensaje>');
	}
  } 
 	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(201,'{"mensaje":"El municipio se agrego correctamente con id='.$cve_municipio.'"}' );	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(201, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El municipio se agrego correctamente con cve_entidad='.$cve_municipio.'</mensaje>');
	}
});

//--------------  POST TEMA ----------------------------//
$app->post ('/entidades/:cve_entidad/municipios/:cve_municipio/temas', function($cve_entidad,$cve_municipio) use ($app){
$accep = $app->request->headers->get('Accept');
$g_body = $app->request->getBody();
 if (value_cve_2($cve_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_entidad no cumple con el formato valido"}');	  
}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400,'<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_entidad no cumple con el formato valido</mensaje>');
	}
  }
 if (value_cve_3($cve_municipio)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_municipio no cumple con el formato valido"}');	  
	}
   else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_municipio no cumple con el formato valido</mensaje>');
	}
  
  }
if($accep ==='aplication/json'){
$body_json=json_decode($g_body);
if (!isset($body_json->tema_3->desc_tema_3)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"El formato JSON es incorecto"}');	  
	}
  }
$nuevo_tema = $body_json->tema_3->desc_tema_3;
}
else{
$body_xml = new DOMDocument('1.0','UTF-8');
$body_xml->loadXML($g_body);
$nodos_tema=$body_xml->getElementsByTagName('desc_tema_3');
if ($nodos_tema->length === 0 ) {
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El formato XML es incorecto</mensaje>');
	}
if($nodos_tema->length ===1){
	$nodo_tema =$nodos_tema->item(0);
	$nuevo_tema =$nodo_tema->nodeValue;
}
}
$verifica_datos = $app->db->query("select * from municipio where cve_municipio='$cve_municipio' and cve_entidad='$cve_entidad'");
if ($verifica_datos->rowCount() == 0 ) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(404,'{"mensaje":"El municipio no existe"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(404, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El dato a modificar no existe</mensaje>');
	}
  }
$verifica_tema = $app->db->query("select * from tercertema where tema_3='$nuevo_tema'");
if ($verifica_tema->rowCount() != 0 ) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"El tema no se puede duplicar"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El tema no se puede duplicar</mensaje>');
	}
  }
$ultimo_id= $app->db->query("select id_tema_3 from tercertema order by id_tema_3 desc limit 1");

if($ultimo_id->rowCount() == 0 ){
$cve_tema="00";
}
else{
foreach ($ultimo_id as $cve) {
    $id_tema_ultimo=$cve['id_tema_3']+1;
  }
	if( $id_tema_ultimo < 10){			
	$cve_tema="0".$id_tema_ultimo;
		}
}
$nuevo_registro= $app->db->query("insert into tercertema values ('$cve_tema','01','$nuevo_tema')");
 if ($nuevo_registro->rowCount() === 0) {
    	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(500,'{"mensaje":"El tema no se pudo agregar"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(500, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El tema no se pudo agregar</mensaje>');
	}
  } 
 	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(201,'{"mensaje":"El tema se agrego correctamente con id='.$cve_tema.'"}' );	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(201, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El tema se agrego correctamente con cve_entidad='.$cve_tema.'</mensaje>');
	}
});



//--------------  POST INDICADOR ----------------------------//
$app->post ('/entidades/:cve_entidad/municipios/:cve_municipio/temas/:cve_tema/indicadores', function($cve_entidad,$cve_municipio,$cve_tema) use ($app){
$accep = $app->request->headers->get('Accept');
$g_body = $app->request->getBody();
 if (value_cve_2($cve_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_entidad no cumple con el formato valido"}');	  
}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400,'<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_entidad no cumple con el formato valido</mensaje>');
	}
  }
 if (value_cve_3($cve_municipio)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_municipio no cumple con el formato valido"}');	  
	}
   else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_municipio no cumple con el formato valido</mensaje>');
	}
  
  }
 if (value_cve_2($cve_tema)) {
  if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La cve_tema no cumple con el formato valido"}');	  
	}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_tema no cumple con el formato valido</mensaje>');
	}
	
  }

if($accep ==='aplication/json'){
$body_json=json_decode($g_body);
if (!isset($body_json->indicador->desc_indicador)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"El formato JSON es incorecto"}');	  
	}
  }
$nuevo_indicador = $body_json->indicador->desc_indicador;
}
else{
$body_xml = new DOMDocument('1.0','UTF-8');
$body_xml->loadXML($g_body);
$nodos_indicador=$body_xml->getElementsByTagName('desc_indicador');
if ($nodos_indicador->length === 0 ) {
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El formato XML es incorecto</mensaje>');
	}
if($nodos_indicador->length ===1){
	$nodo_indicador =$nodos_indicador->item(0);
	$nuevo_indicador =$nodo_indicador->nodeValue;
}
}
$verifica_datos = $app->db->query("select * from municipio where cve_municipio='$cve_municipio' and cve_entidad='$cve_entidad'");
if ($verifica_datos->rowCount() == 0 ) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(404,'{"mensaje":"La entidad o municipio no existe"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(404, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La entidad o municipio no existe</mensaje>');
	}
  }
$verifica_tema = $app->db->query("select * from tercertema where id_tema_3='$cve_tema'");
if ($verifica_tema->rowCount() == 0 ) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"El tema no existe"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El tema no existe</mensaje>');
	}
  }
$verifica_datos = $app->db->query("select * from indicador where indicador='$nuevo_indicador'");
if ($verifica_datos->rowCount() != 0 ) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"El indicador no se puede duplicar"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El indicador no se puede duplicar</mensaje>');
	}
  }

$ultimo_id= $app->db->query("select id_indicador from indicador order by id_indicador desc limit 1");

foreach ($ultimo_id as $cve) {
    $id_indicador=$cve['id_indicador']+1;
  }

$nuevo_registro= $app->db->query("insert into indicador values ('$id_indicador','$nuevo_indicador')");
 if ($nuevo_registro->rowCount() === 0) {
    	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(500,'{"mensaje":"El indicador no se pudo agregar"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(500, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El indicador no se pudo agregar</mensaje>');
	}
  } 
 	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(201,'{"mensaje":"El indicador se agrego correctamente con id='.$id_indicador.'"}' );	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(201, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El indicador se agrego correctamente con cve_entidad='.$id_indicador.'</mensaje>');
	}
});

//------------------------------------------------------- PUTS -------------------------------------------------------//
//--------------------- PUT Entidad ----------------------------------//
$app->put ('/entidades/:cve_entidad', function($cve_entidad) use ($app){
$accep = $app->request->headers->get('Accept');
$g_body = $app->request->getBody();
if (value_cve_2($cve_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La cve_entidad no cumple con el formato valido"}');	  
}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400,'<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_entidad no cumple con el formato valido</mensaje>');
	}
  }
if($accep ==='aplication/json'){
$body_json=json_decode($g_body);
if (!isset($body_json->entidad->desc_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"El formato JSON es incorecto"}');	  
	}
  }
$nueva_entidad = $body_json->entidad->desc_entidad;
}
else{
$body_xml = new DOMDocument('1.0','UTF-8');
$body_xml->loadXML($g_body);
$nodos_entidad=$body_xml->getElementsByTagName('desc_entidad');
if ($nodos_entidad->length === 0 ) {
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El formato XML es incorecto</mensaje>');
	}
if($nodos_entidad->length ===1){
	$nodo_entidad =$nodos_entidad->item(0);
	$nueva_entidad =$nodo_entidad->nodeValue;
}
}
$verifica_cve = $app->db->query("select * from entidad where cve_entidad='$cve_entidad'");
if ($verifica_cve->rowCount() == 0 ) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(404,'{"mensaje":"La entidad a modificar no existe"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(404, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La entidad a modificar no existe</mensaje>');
	}	
  }
$verifica_entidad = $app->db->query("select * from entidad where desc_entidad='$nueva_entidad'");
if ($verifica_entidad->rowCount() != 0 ) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La entidad no se puede duplicar"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La entidad no se puede duplicar</mensaje>');
	}
	
  }
$actualiza_registro= $app->db->query("update entidad set desc_entidad='$nueva_entidad' where cve_entidad='$cve_entidad'");

 if ($actualiza_registro->rowCount() === 0) {
    	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(500,'{"mensaje":"El registro no se pudo actualizar"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(500, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El registro no se pudo actualizar</mensaje>');
	}
  } 
if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(201,'{"mensaje":"La entidad se actualizo correctamente"}' );	  
	}
else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(201, "<?xml version='1.0' encoding='UTF-8'?><mensaje>La entidad se actualizo correctamente</mensaje>");
	}
});



//--------------  PUT MUNICIPIO ----------------------------//
$app->put ('/entidades/:cve_entidad/municipios/:cve_municipio', function($cve_entidad,$cve_municipio) use ($app){
$accep = $app->request->headers->get('Accept');
$g_body = $app->request->getBody();
if (value_cve_2($cve_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La cve_entidad no cumple con el formato valido"}');	  
}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400,'<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_entidad no cumple con el formato valido</mensaje>');
	}
  }
 if (value_cve_3($cve_municipio)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_municipio no cumple con el formato valido"}');	  
	}
   else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_municipio no cumple con el formato valido</mensaje>');
	}
  }
if($accep ==='aplication/json'){
$body_json=json_decode($g_body);
if (!isset($body_json->municipio->desc_municipio)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"El formato JSON es incorecto"}');	  
	}
  }
$nuevo_municipio = $body_json->municipio->desc_municipio;
}
else{
$body_xml = new DOMDocument('1.0','UTF-8');
$body_xml->loadXML($g_body);
$nodos_municipio=$body_xml->getElementsByTagName('desc_municipio');
if ($nodos_municipio->length === 0 ) {
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El formato XML es incorecto</mensaje>');
	}
if($nodos_municipio->length ===1){
	$nodo_municipio =$nodos_municipio->item(0);
	$nuevo_municipio =$nodo_municipio->nodeValue;
}
}
$verifica_datos = $app->db->query("select * from municipio where cve_municipio='$cve_municipio' and cve_entidad='$cve_entidad'");
if ($verifica_datos->rowCount() == 0 ) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(404,'{"mensaje":"El municipio a modificar no existe"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(404, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El municipio a modificar no existe</mensaje>');
	}
  }
$verifica_duplicado = $app->db->query("select * from municipio where cve_entidad='$cve_entidad' and desc_municipio='$nuevo_municipio'");
if ($verifica_duplicado->rowCount() != 0 ) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"El municipio no se puede duplicar"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El municipio no se puede duplicar</mensaje>');
	}
  }

$actualiza_registro= $app->db->query("update municipio set desc_municipio='$nuevo_municipio' where cve_municipio='$cve_municipio' and cve_entidad='$cve_entidad'");
 if ($actualiza_registro->rowCount() === 0) {
    	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(500,'{"mensaje":"El municipio no se pudo modificar"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(500, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El municipio no se pudo modificar</mensaje>');
	}
  } 
if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(201,'{"mensaje":"El municipio se modifico correctamente"}' );	  
	}
else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(201, "<?xml version='1.0' encoding='UTF-8'?><mensaje>El municipio se modifico correctamente</mensaje>");
	}
});


//--------------  PUT TEMA ----------------------------//
$app->put ('/entidades/:cve_entidad/municipios/:cve_municipio/temas/:cve_tema', function($cve_entidad,$cve_municipio,$cve_tema) use ($app){
$accep = $app->request->headers->get('Accept');
$g_body = $app->request->getBody();
 if (value_cve_2($cve_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_entidad no cumple con el formato valido"}');	  
}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400,'<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_entidad no cumple con el formato valido</mensaje>');
	}
  }
 if (value_cve_3($cve_municipio)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_municipio no cumple con el formato valido"}');	  
	}
   else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_municipio no cumple con el formato valido</mensaje>');
	}  
  }
 if (value_cve_2($cve_tema)) {
  if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_tema no cumple con el formato valido"}');	  
	}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_tema no cumple con el formato valido</mensaje>');
	}
}
if($accep ==='aplication/json'){
$body_json=json_decode($g_body);
if (!isset($body_json->tema_3->desc_tema_3)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"El formato JSON es incorecto"}');	  
	}
  }
$nuevo_tema = $body_json->tema_3->desc_tema_3;
}
else{
$body_xml = new DOMDocument('1.0','UTF-8');
$body_xml->loadXML($g_body);
$nodos_tema=$body_xml->getElementsByTagName('desc_tema_3');
if ($nodos_tema->length === 0 ) {
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El formato XML es incorecto</mensaje>');
	}
if($nodos_tema->length ===1){
	$nodo_tema =$nodos_tema->item(0);
	$nuevo_tema =$nodo_tema->nodeValue;
}
}
$verifica_datos = $app->db->query("select * from municipio where cve_municipio='$cve_municipio' and cve_entidad='$cve_entidad'");
if ($verifica_datos->rowCount() == 0 ) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(404,'{"mensaje":"El dato a modificar no existe"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(404, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El dato a modificar no existe</mensaje>');
	}
  }

$verifica_tema = $app->db->query("select * from tercertema where id_tema_3='$cve_tema'");
if ($verifica_tema->rowCount() == 0 ) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"El dato a modificar no existe"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>"El dato a modificar no existe"</mensaje>');
	}
  }
$verifica_tema = $app->db->query("select * from tercertema where tema_3='$nuevo_tema'");
if ($verifica_tema->rowCount() != 0 ) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"El tema no se puede duplicar"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El tema no se puede duplicar</mensaje>');
	}
  }
$nuevo_registro= $app->db->query("update tercertema set tema_3='$nuevo_tema' where id_tema_3='$cve_tema'");
 if ($nuevo_registro->rowCount() === 0) {
    	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(500,'{"mensaje":"El tema no se pudo agregar"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(500, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El tema no se pudo agregar</mensaje>');
	}
  } 
 	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(201,'{"mensaje":"El tema se modifico correctamente"}' );	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(201, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El tema se modifico correctamente</mensaje>');
	}
});





//--------------  PUT INDICADOR ----------------------------//
$app->put ('/entidades/:cve_entidad/municipios/:cve_municipio/temas/:cve_tema/indicadores/:cve_indicador/', function($cve_entidad,$cve_municipio,$cve_tema,$cve_indicador) use ($app){
$accep = $app->request->headers->get('Accept');
$g_body = $app->request->getBody();
 if (value_cve_2($cve_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_entidad no cumple con el formato valido"}');	  
}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400,'<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_entidad no cumple con el formato valido</mensaje>');
	}
  }
 if (value_cve_3($cve_municipio)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_municipio no cumple con el formato valido"}');	  
	}
   else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_municipio no cumple con el formato valido</mensaje>');
	}
  
  }
 if (value_cve_2($cve_tema)) {
  if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La cve_tema no cumple con el formato valido"}');	  
	}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_tema no cumple con el formato valido</mensaje>');
	}
	
  }
if (value_cve_10($cve_indicador)) {
  if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"La cve_indicador no cumple con el formato valido"}');	  
	}
  else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_indicador no cumple con el formato valido</mensaje>');
	}
	
  }

if($accep ==='aplication/json'){
$body_json=json_decode($g_body);
if (!isset($body_json->indicador->desc_indicador)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"El formato JSON es incorecto"}');	  
	}
  }
$nuevo_indicador = $body_json->indicador->desc_indicador;
}
else{
$body_xml = new DOMDocument('1.0','UTF-8');
$body_xml->loadXML($g_body);
$nodos_indicador=$body_xml->getElementsByTagName('desc_indicador');
if ($nodos_indicador->length === 0 ) {
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El formato XML es incorecto</mensaje>');
	}
if($nodos_indicador->length ===1){
	$nodo_indicador =$nodos_indicador->item(0);
	$nuevo_indicador =$nodo_indicador->nodeValue;
}
}
$verifica_datos = $app->db->query("select * from municipio where cve_municipio='$cve_municipio' and cve_entidad='$cve_entidad'");
if ($verifica_datos->rowCount() == 0 ) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(404,'{"mensaje":"La entidad o municipio no existe"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(404, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La entidad o municipio no existe</mensaje>');
	}
  }
$verifica_tema = $app->db->query("select * from tercertema where id_tema_3='$cve_tema'");
if ($verifica_tema->rowCount() == 0 ) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"El tema no existe"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El tema no existe</mensaje>');
	}
  }
$verifica_datos = $app->db->query("select * from indicador where indicador='$nuevo_indicador'");
if ($verifica_datos->rowCount() != 0 ) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"El indicador no se puede duplicar"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El indicador no se puede duplicar</mensaje>');
	}
  }


$nuevo_registro= $app->db->query("update indicador set indicador='$nuevo_indicador' where id_indicador='$cve_indicador'");
 if ($nuevo_registro->rowCount() === 0) {
    	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(500,'{"mensaje":"El indicador no se pudo modificar"}');	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(500, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El indicador no se pudo modificar</mensaje>');
	}
  } 
 	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(201,'{"mensaje":"El indicador se modifico correctamente"}' );	  
	}
 	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(201, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El indicador se modifico correctamente</mensaje>');
	}
});



//--------------------------------------------------------------------------DELETE----------------------------------------------------------------------//
//--------------------Todas las Entidades--------------------------------//
$app->delete('/entidades',
function() use($app) {
$accep = $app->request->headers->get('Accept');

$d_finanzas = $app->db->query("delete from finanzas ");

$d_indicador = $app->db->query("delete from indicador");

$d_municicpios = $app->db->query("delete from municipio");

$d_temas = $app->db->query("delete from tercertema ");

$d_entidad = $app->db->query("delete from entidad ");


	if($accep=='aplication/xml'){
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(200, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El dato se borro correctamente</mensaje>');
	}
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(200, '{"mensaje":"El dato se borro correctamente"}');
	}
});


//--------------------UNA Entidad--------------------------------//
$app->delete('/entidades/:cve_entidad',
function($cve_entidad) use($app) {
$accep = $app->request->headers->get('Accept');
 if (value_cve_2($cve_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La cve_entidad no cumple con el formato valido"}');	  
}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400,'<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_entidad no cumple con el formato valido</mensaje>');
	}
  }

$verifica = $app->db->query("select * from entidad where cve_entidad='$cve_entidad'");
if ($verifica->rowCount() == 0 ) {
     $app->response->headers->set('Content-Type', $accep);
	$app->halt(404, noexiste($accep));
  }

$d_finanzas = $app->db->query("delete from finanzas where cve_entidad='$cve_entidad'");

if ($d_finanzas->rowCount() != 0) {
    $app->response->headers->set('Content-Type', $accep);
	$app->halt(404, noexiste($accep));
  }

$d_municicpios = $app->db->query("delete from municipio where cve_entidad='$cve_entidad'");

$d_entidad = $app->db->query("delete from entidad  where cve_entidad='$cve_entidad'");


	if($accep=='aplication/xml'){
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(200, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El dato se borro correctamente</mensaje>');
	}
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(200, '{"mensaje":"El dato se borro correctamente"}');
	}
});



//-------------------- Todos los municipios de una entidad--------------------------------//
$app->delete('/entidades/:cve_entidad/municipios',
function($cve_entidad) use($app) {
$accep = $app->request->headers->get('Accept');
 if (value_cve_2($cve_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La cve_entidad no cumple con el formato valido"}');	  
}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400,'<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_entidad no cumple con el formato valido</mensaje>');
	}
  }

$verifica = $app->db->query("select * from entidad where cve_entidad='$cve_entidad'");
if ($verifica->rowCount() == 0 ) {
     $app->response->headers->set('Content-Type', $accep);
	$app->halt(404, noexiste($accep));
  }

$d_finanzas = $app->db->query("delete from finanzas where cve_entidad='$cve_entidad'");

if ($d_finanzas->rowCount() != 0) {
    $app->response->headers->set('Content-Type', $accep);
	$app->halt(404, noexiste($accep));
  }

$d_municicpios = $app->db->query("delete from municipio where cve_entidad='$cve_entidad'");


	if($accep=='aplication/xml'){
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(200, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El dato se borro correctamente</mensaje>');
	}
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(200, '{"mensaje":"El dato se borro correctamente"}');
	}
});


//-------------------- Un Municipio--------------------------------//
$app->delete('/entidades/:cve_entidad/municipios/:cve_municipio',
function($cve_entidad,$cve_municipio) use($app) {
$accep = $app->request->headers->get('Accept');
 if (value_cve_2($cve_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La cve_entidad no cumple con el formato valido"}');	  
}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400,'<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_entidad no cumple con el formato valido</mensaje>');
	}
  }

$verifica = $app->db->query("select * from municipio where cve_entidad='$cve_entidad' and cve_municipio='$cve_municipio'");
if ($verifica->rowCount() == 0 ) {
     $app->response->headers->set('Content-Type', $accep);
	$app->halt(404, noexiste($accep));
  }


$d_finanzas = $app->db->query("delete from finanzas where cve_entidad='$cve_entidad' and cve_municipio='$cve_municipio'");

if ($d_finanzas->rowCount() != 0) {
    $app->response->headers->set('Content-Type', $accep);
	$app->halt(404, noexiste($accep));
  }

$d_municicpios = $app->db->query("delete from municipio where cve_entidad='$cve_entidad' and cve_municipio='$cve_municipio'");


	if($accep=='aplication/xml'){
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(200, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El dato se borro correctamente</mensaje>');
	}
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(200, '{"mensaje":"El dato se borro correctamente"}');
	}
});



//--------------------UN INDICADOR ESPECIFICO--------------------------------//
$app->delete('/entidades/:cve_entidad/municipios/:cve_municipio/temas/:id_tema_3/indicadores/:cve_indicador',
function($cve_entidad,$cve_municipio,$id_tema_3,$cve_indicador) use($app) {
$accep = $app->request->headers->get('Accept');
 if (value_cve_2($cve_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La cve_entidad no cumple con el formato valido"}');	  
}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400,'<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_entidad no cumple con el formato valido</mensaje>');
	}
  }
 if (value_cve_3($cve_municipio)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La cve_municipio no cumple con el formato valido"}');	  
	}
   else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_municipio no cumple con el formato valido</mensaje>');
	}
  
  }
 if (value_cve_2($id_tema_3)) {
  if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La cve_tema no cumple con el formato valido"}');	  
	}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_tema no cumple con el formato valido</mensaje>');
	}
	
  }
if (value_cve_10($cve_indicador)) {
  if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La cve_indicador no cumple con el formato valido"}');	  
	}
  else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_indicador no cumple con el formato valido</mensaje>');
	}
	
  }

$verifica = $app->db->query("select * from indicador where id_indicador='$cve_indicador'");
if ($verifica->rowCount() == 0 ) {
     $app->response->headers->set('Content-Type', $accep);
	$app->halt(404, noexiste($accep));
  }

$d_finanzas = $app->db->query("delete from finanzas where cve_entidad='$cve_entidad' and cve_municipio='$cve_municipio' and id_tema_3='$id_tema_3' and id_indicador='$cve_indicador'");

if ($d_finanzas->rowCount() != 0) {
    $app->response->headers->set('Content-Type', $accep);
	$app->halt(404, noexiste($accep));
  }

$d_valores = $app->db->query("delete from indicador where id_indicador='$cve_indicador'");


	if($accep=='aplication/xml'){
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(200, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El dato se borro correctamente</mensaje>');
	}
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(200, '{"mensaje":"El dato se borro correctamente"}');
	}
});


//--------------------BORRA UN VALOR DE UN AÑO EN ESPECIFICO--------------------------------//
$app->delete('/entidades/:cve_entidad/municipios/:cve_municipio/temas/:id_tema_3/indicadores/:id_indicador/',
function($cve_entidad,$cve_municipio,$id_tema_3,$id_indicador) use($app) {
$accep = $app->request->headers->get('Accept');
$periodo = $app->request->params('periodo');
 if (value_cve_2($cve_entidad)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La cve_entidad no cumple con el formato valido"}');	  
}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400,'<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_entidad no cumple con el formato valido</mensaje>');
	}
  }
 if (value_cve_3($cve_municipio)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La cve_municipio no cumple con el formato valido"}');	  
	}
   else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_municipio no cumple con el formato valido</mensaje>');
	}
  
  }
 if (value_cve_2($id_tema_3)) {
  if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La cve_tema no cumple con el formato valido"}');	  
	}
	else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_tema no cumple con el formato valido</mensaje>');
	}
	
  }
if (value_cve_10($id_indicador)) {
  if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(400,'{"mensaje":"La cve_indicador no cumple con el formato valido"}');	  
	}
  else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>La cve_indicador no cumple con el formato valido</mensaje>');
	}
	
  }
if (value_anio($periodo)) {
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->response->setStatus(400);	
	  $app->halt(400,'{"mensaje":"El periodo no cumple con el formato valido"}');	  
	}
 	 else{
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(400, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El periodo no cumple con el formato valido</mensaje>');
	}

  }
$verifica = $app->db->query("select * from anio where anio='$periodo'");
if ($verifica->rowCount() == 0 ) {
     $app->response->headers->set('Content-Type', $accep);
	$app->halt(404, noexiste($accep));
  }
$tvalores = $app->db->query("delete from finanzas where cve_entidad='$cve_entidad' and cve_municipio='$cve_municipio' and id_tema_3='$id_tema_3' 
and id_indicador='$id_indicador' and id_anio=(select id_anio from anio where anio='$periodo' limit 1)");

if ($tvalores->rowCount() == 0) {
    $app->response->headers->set('Content-Type', $accep);
	$app->halt(404, noexiste($accep));
  }
	if($accep=='aplication/xml'){
	  $app->response->headers->set('Content-Type', 'application/xml');
	  $app->halt(200, '<?xml version="1.0" encoding="UTF-8"?><mensaje>El dato se borro correctamente</mensaje>');
	}
	if($accep=='aplication/json'){
	  $app->response->headers->set('Content-Type', 'application/json');
	  $app->halt(200, '{"mensaje":"El dato se borro correctamente"}');
	}
});





