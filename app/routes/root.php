<?php

$app->get('/', function() use($app) {
  $datos = array(
    'titulo' => 'FINANZAS PÚBLICAS Y ESTATALES',
  );
  $app->render('root.php', $datos);
});
