<?php $route="hpw.economia.nacional/api"?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>FINANZAS PUBLICAS ESTATALES Y MUNICIPALES</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
<link rel="stylesheet" href="../css/main.css">
<script src="../js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>

</head>
<body>

  <!--[if lt IE 7]>
  <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->
<div class="row">
	<div class="col-md-2">
		<img src="../img/info_financiera_top1.jpg" class="img-thumbnail" alt="Responsive image">
	</div>
	<div class="col-md-8">
		<h3><strong><p class="text-center">FINANZAS PUBLICAS ESTATALES Y MUNICIPALES</p></strong></h3>
	</div>
	<div class="col-md-2">
		<img src="../img/info_financiera_top2.jpg" class="img-thumbnail" alt="Responsive image">
	</div>	
</div>

<div class="navbar navbar-default" role="navigation">
	<div class="col-md-6 col-md-offset-6">.
		<div class="container-fluid">
			<div class="navbar-header">
		    	<a class="navbar-brand" href="/"><span class="glyphicon glyphicon-home"></span> Inicio</a>
		  	</div>
			<div class="navbar-header">
		    	<a class="navbar-brand" href="/about/">Acerca de</a>
		  	</div>
			<div class="navbar-header">
		    	<a class="navbar-brand" href="/api/">API</a>
		  	</div>
		  	<div class="navbar-header">
		  	  <a class="navbar-brand" href="../docs/">Documentación</a>
		  	</div>
		</div>
	</div>
</div>

<hr/>

<h2><strong><p class="text-center">Documentación</p></strong></h2>


<div class="container-fluid">	
	<div>
		<div>
      		<h2>Entidades</h2>
		    <ul>
		        <li><strong>Endpoint/URL: </strong><a href="../api/entidades">http://<?php echo $route?>/entidades</a></li>
		        <li><strong>Descripci&oacute;n: </strong> Regresa todas las entidades.</li>
		        <li><strong>M&eacute;todos HTTP: </strong> GET, POST, DELETE</li>
		        <li><strong>Par&aacute;metros:</strong> ninguno</li>
		        <li><strong>Headers: </strong> <br>Accept: aplication/xml <br>
		        Accept: aplication/json
		        </li>
				<div class="row"><div class="col-md-6">
				<li><strong>Ejemplo XML:</strong> 
				<pre>&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;entidades&gt;
  &lt;entidad id="00"&gt;
    &lt;desc_entidad&gt;Nacional&lt;/desc_entidad&gt;
  &lt;/entidad&gt;
  .
  .
  .
&lt;/entidades&gt;






</pre>
				</li></div><div class="col-md-6">
				<li><strong>Ejemplo JSON:</strong> 
				<pre>
{
"entidades":[
             {
              "entidad":{
                          "id":"00",
                          "desc_entidad":"Nacional"
                         }
              }
              .
              .
              .
            ]
}
				
				</pre>
				</li>
				</div>
				</div>
				<li><strong>M&eacute;todo POST : </strong></li><div class="row"><div class="col-md-6"><strong>Body XML: </strong>
				<pre>
				
&lt;entidad&gt;
    &lt;desc_entidad&gt;Datos&lt;/desc_entidad&gt;
&lt;/entidad&gt;
				
				
				</pre></div><div class="col-md-6"><strong>Body JSON: </strong>
				<pre>
{
"entidad":{
           "desc_entidad": "Datos"
           }
}
				
				</pre> </div></div>
				<li><strong>Errores y Mensajes GET :</strong></li> 
				C&oacute;digo de estado HTTP 404: Recurso no encontrado.<br>
				<li><strong>Errores y Mensajes POST :</strong></li> 
				C&oacute;digo de estado HTTP 201: La entidad se agrego correctamente con cve_entidad="ID".<br>
				C&oacute;digo de estado HTTP 400: El formato XML es incorrecto.<br>
				C&oacute;digo de estado HTTP 400: El formato JSON es incorrecto.<br>
				<li><strong>Errores y Mensajes DELETE :</strong></li> 
				C&oacute;digo de estado HTTP 200: EL dato se borro correctamente.<br>
				
				
			</ul>
		</div>
		<hr>
		<div>
			<h2>Entidad en espec&iacute;fico</h2>
			<ul>
			<li><strong>Endpoint/URL:</strong> <a href="../api/entidades/00">http://<?php echo $route?>/entidades/cve_entidad</a></li>
			 	<li><strong>Descripci&oacute;n:</strong> Regresa una Entidad en espec&iacute;fico.</li>
			 	<li><strong>M&eacute;todos HTTP:</strong> GET, PUT, DELETE.</li>
			 	<li><strong>Presentaci&oacute;n de resultados:</strong> XML y JSON</li>
				<li><strong>Par&aacute;metros:</strong> <strong>cve_entidad</strong>.</li>
				<li><strong>Headers: </strong> <br>Accept: aplication/xml <br>
		        Accept: aplication/json
		        </li>
				<div class="row">
				<div class="col-md-6">
				<li><strong>Ejemplo XML:</strong> 
				<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
  &lt;entidad id="00"&gt;
    &lt;desc_entidad&gt;Nacional&lt;/desc_entidad&gt;
  &lt;/entidad&gt;
  
  
</pre>
				</li>
				</div>
				<div class="col-md-6">
				<li><strong>Ejemplo JSON:</strong> 
				<pre>
{
"entidad":{
            "id":"00",
            "desc_entidad":"Nacional"
          }
}</pre>
				</li>
				</div>
				</div>
								<li><strong>M&eacute;todo  PUT : </strong></li><div class="row"><div class="col-md-6"><strong>Body XML: </strong>
				<pre>
				
&lt;entidad&gt;
    &lt;desc_entidad&gt;Datos&lt;/desc_entidad&gt;
&lt;/entidad&gt;
				
				
				</pre></div><div class="col-md-6"><strong>Body JSON: </strong>
				<pre>
{
"entidad":{
           "desc_entidad": "Datos"
           }
}
				
				</pre> </div></div>
				<li><strong>Errores y Mensajes GET :</strong> </li>
				C&oacute;digo de estado HTTP 400: La <strong>cve_entidad</strong> no cumple con el formato v&aacute;lido.<br>
				C&oacute;digo de estado HTTP 404: Recurso no encontrado.
				<li><strong>Errores y Mensajes PUT :</strong> </li>
				C&oacute;digo de estado HTTP 201: La entidad se actualizo correctamente.<br>
				C&oacute;digo de estado HTTP 400: El formato XML es incorrecto.<br>
				C&oacute;digo de estado HTTP 400: El formato JSON es incorrecto.<br>
				<li><strong>Errores y Mensajes DELETE :</strong></li> 
				C&oacute;digo de estado HTTP 200: EL dato se borro correctamente.<br>
			</ul>
		</div>
		<hr>
		<div>
			<h2>Municipios</h2>
			<ul>
			<li><strong>Endpoint/URL:</strong> <a href="../api/entidades/00/municipios">http://<?php echo $route?>/entidades/cve_entidad/municipios</a></li>
			 	<li><strong>Descripci&oacute;n:</strong> Regresa todos los municipios de una entidad.</li>
			 	<li><strong>M&eacute;todos HTTP:</strong> GET, POST, DELETE.</li>
			 	<li><strong>Presentaci&oacute;n de resultados:</strong> XML y JSON.</li>
				<li><strong>Par&aacute;metros:</strong> <strong>cve_entidad</strong>.</li>
				<li><strong>Headers: </strong> <br>Accept: aplication/xml <br>
		        Accept: aplication/json
		        </li>
				<div class="row">
				<div class="col-md-6">
				<li><strong>Ejemplo XML:</strong> 
				<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;municipios&gt;
	&lt;municipio id="000"&gt;
		&lt;desc_municipio&gt;Nacional&lt;/desc_municipio&gt;
	&lt;/municipio&gt;
	.
	.
	.
&lt;/municipios&gt;




</pre>
				</li>
				</div>
				<div class="col-md-6">
				<li><strong>Ejemplo JSON:</strong> 
				<pre>
{ 
"municipios":[
              {
               "municipio":{ 
                            "id":"000",
                            "desc_municipio":"Nacional"
                           }
               }
               .
               .
               .
              ]
}</pre>
				
				</div>
				</div>
				
				<li><strong>M&eacute;todo POST : </strong></li><div class="row"><div class="col-md-6"><strong>Body XML: </strong>
				<pre>
				
&lt;municipio&gt;
    &lt;des_municipio&gt;Datos&lt;/des_municipio&gt;
&lt;/municipio&gt;
				
				
				</pre></div><div class="col-md-6"><strong>Body JSON: </strong>
				<pre>
{
"municipio":{
           "des_municipio": "Datos"
           }
}
				
				</pre> </div></div>	
				
				<li><strong>Errores y Mensajes GET :</strong></li> 
				C&oacute;digo de estado HTTP 400: Solicitud incorrecta. Mensajes La <strong>cve_entidad</strong> no cumple con el formato v&aacute;lido.<br>
				C&oacute;digo de estado HTTP 404: Recurso no encontrado.<br>
				<li><strong>Errores y Mensajes POST :</strong></li> 
				C&oacute;digo de estado HTTP 201: El municipio se agrego correctamente con cve_municipio="ID".<br>
				C&oacute;digo de estado HTTP 400: El formato XML es incorrecto.<br>
				C&oacute;digo de estado HTTP 400: El formato JSON es incorrecto.<br>
				<li><strong>Errores y Mensajes DELETE:</strong></li> 
				C&oacute;digo de estado HTTP 200 : EL dato se borro correctamente.<br>
				
						
				
			</ul>
		</div>
		<hr>
		<div>
			<h2>Municipio en espec&iacute;fico</h2>
			<ul>
			    <li><strong>Endpoint/URL:</strong> <a href="../api/entidades/00/municipios/000">http://<?php echo $route?>/cve_entidad/municipios/cve_municipio</a> </li>
			 	<li><strong>Descripci&oacute;n:</strong> Regresa un Municipio en espec&iacute;fico</li>
			 	<li><strong>M&eacute;todos HTTP:</strong> GET, PUT, DELETE.</li>
			 	<li><strong>Presentaci&oacute;n de resultados:</strong> XML y JSON.</li>
				<li><strong>Par&aacute;metros:</strong> <strong>cve_entidad</strong>, <strong>cve_municipio</strong>.</li>
				<li><strong>Headers: </strong> <br>Accept: aplication/xml <br>
		        Accept: aplication/json
		        </li>
				<div class="row">
				<div class="col-md-6">
				<li><strong>Ejemplo XML: </strong>
				<pre>&lt;?xml version="1.0" encoding="UTF-8"?&gt;
	&lt;municipio id="000"&gt;
		&lt;desc_municipio&gt;Nacional&lt;/desc_municipio&gt;
	&lt;/municipio&gt;
	
	</pre>
				</li>
				</div>
				<div class="col-md-6">
				<li><strong>Ejemplo JSON:</strong>
				<pre>
{
"municipio":{
             "id":"000",
             "desc_municipio":"Nacional"
             }
}</pre></li>
</div>
</div>
				<li><strong>M&eacute;todo PUT : </strong></li><div class="row"><div class="col-md-6"><strong>Body XML: </strong>
				<pre>
				
&lt;municipio&gt;
    &lt;des_municipio&gt;Datos&lt;/des_municipio&gt;
&lt;/municipio&gt;
				
				
				</pre></div><div class="col-md-6"><strong>Body JSON: </strong>
				<pre>
{
"municipio":{
           "des_municipio": "Datos"
           }
}
				
				</pre> </div></div>	
				
				<li><strong>Errores y Mensajes GET :</strong></li> 
				C&oacute;digo de estado HTTP 400:  La <strong>cve_entidad</strong> no cumple con el formato v&aacute;lido.<br>
				C&oacute;digo de estado HTTP 400: La <strong>cve_municipio</strong> no cumple con el formato v&aacute;lido.<br>				
				C&oacute;digo de estado HTTP 404: Recurso no encontrado.<br>
				<li><strong>Errores y Mensajes PUT :</strong></li> 
				C&oacute;digo de estado HTTP 201: El municipio se actualizo correctamente.<br>
				C&oacute;digo de estado HTTP 400: El formato XML es incorrecto.<br>
				C&oacute;digo de estado HTTP 400: El formato JSON es incorrecto.<br>
				<li><strong>Errores y Mensajes DELETE :</strong></li> 
				C&oacute;digo de estado HTTP 200: EL dato se borro correctamente.<br>
				
			</ul>
		</div>
		<hr>
		<div>
      		<h2>Temas</h2>
			<ul>
			    <li><strong>Endpoint/URL:</strong> <a href="../api/entidades/00/municipios/000/temas">http://<?php echo $route?>/entidades/cve_entidad/municipios/cve_municipio/temas</a></li>
				<li><strong>Descripci&oacute;n:</strong> Regresa todos los temas de un municipio</li>
				<li><strong>M&eacute;todos HTTP:</strong> GET, POST, DELETE.</li>
			 	<li><strong>Presentaci&oacute;n de resultados:</strong> XML y JSON.</li>
				<li><strong>Par&aacute;metros:</strong> <strong>cve_entidad</strong>, <strong>cve_municipio</strong>.</li>
				<li><strong>Headers: </strong> <br>Accept: aplication/xml <br>
		        Accept: aplication/json
		        </li>
				<div class="row">
				<div class="col-md-6">
				<li><strong>Ejemplo XML:</strong> 
				<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
    &lt;tema_1 id="01"&gt;
        &lt;desc_tema_1>Economia&lt;/desc_tema_1&gt;
            &lt;tema_2 id="01"&gt;
                &lt;desc_tema_2&gt;Finanzas publicas&lt;/desc_tema_2&gt;
                    &lt;temas_3&gt;
                        &lt;tema_3 id="01"&gt;
                            &lt;desc_tema_3&gt;Finanzas publicas&lt;/desc_tema_3&gt;
                        &lt;/tema_3&gt;
                        .
                        .
                        .
                    &lt;/temas_3&gt;
            &lt;/tema_2&gt;
    &lt;/tema_1&gt;
   
				
				
				
				
				
				
				</pre>
				
				</li>
				</div>
				<div class="col-md-6">
				<li><strong>Ejemplo JSON:</strong> 
				<pre>
{
 "tema_1":{
            "id":"01",
            "desc_tema_1":"Economia",
            "tema_2":{
                      "id":"01",
                      "desc_tema_2":"Finanzas publicas",
                      "temas_3":[
                                {
                                "tema_3":{
                                          "id":"01",
                                          "desc_tema_3":"Finanzas publicas"
                                          }
                                 .
                                 .
                                 .
                                 }
                                 ]
                      }
            }
}
				</pre>
				</li></div></div>

				<li><strong>M&eacute;todo POST : </strong></li><div class="row"><div class="col-md-6"><strong>Body XML: </strong>
				<pre>
				
&lt;tema_3&gt;
    &lt;des_tema_3&gt;Datos&lt;/des_tema_3&gt;
&lt;/tema_3&gt;
				
				
				</pre></div><div class="col-md-6"><strong>Body JSON: </strong>
				<pre>
{
"tema_3":{
           "des_tema_3": "Datos"
           }
}
				
				</pre> </div></div>	
				
				<li><strong>Errores y Mensajes GET:</strong></li> 
				C&oacute;digo de estado HTTP 400:  La <strong>cve_entidad</strong> no cumple con el formato v&aacute;lido.<br>
				C&oacute;digo de estado HTTP 400:  La <strong>cve_municipio</strong> no cumple con el formato v&aacute;lido.<br>				
				C&oacute;digo de estado HTTP 404: Recurso no encontrado.<br>
				<li><strong>Errores y Mensajes PUT:</strong></li> 
				C&oacute;digo de estado HTTP 201: El tema se agrego correctamente con cve_tema="ID".<br>
				C&oacute;digo de estado HTTP 400: El formato XML es incorrecto.<br>
				C&oacute;digo de estado HTTP 400: El formato JSON es incorrecto.<br>
				<li><strong>Errores y Mensajes DELETE:</strong></li> 
				C&oacute;digo de estado HTTP 200: EL dato se borro correctamente.<br>				
				
										
			</ul>
		</div>
		<hr>
		<div>
      		<h2>Tema en espec&iacute;fico</h2>
			<ul>
			    <li><strong>Endpoint/URL:</strong> <a href="../api/entidades/00/municipios/000/temas/01">http://<?php echo $route?>/entidades/cve_entidad/municipios/cve_municipio/temas/cve_tema</a> </li>
				<li><strong>Descripci&oacute;n:</strong> Regresa un tema en espec&iacute;fico</li>
				<li><strong>M&eacute;todos HTTP:</strong> GET, PUT, DELETE.</li>
			 	<li><strong>Presentaci&oacute;n de resultados:</strong> XML y JSON.</li>
				<li><strong>Par&aacute;metros:</strong> <strong>cve_tema</strong>, <strong>cve_entidad</strong>, <strong>cve_municipio</strong>.</li>
				<li><strong>Headers: </strong> <br>Accept: aplication/xml <br>
		        Accept: aplication/json
		        </li>
				
				
				
				 
				<div class="row">
				<div class="col-md-6"><li><strong>Ejemplo XML:</strong> <pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
	&lt;tema_1 id="01"&gt;
		&lt;desc_tema_1&gt;Economia&lt;/desc_tema_1&gt;
		&lt;tema_2 id="01"&gt;
			&lt;desc_tema_2&gt;Finanzas publicas&lt;/desc_tema_2&gt;
			&lt;tema_3 id="01"&gt;
				&lt;desc_tema_3&gt;Finanzas publicas&lt;/desc_tema_3&gt;
			&lt;/tema_3&gt;
		&lt;/tema_2&gt;
	&lt;/tema_1&gt;
	
	
	
	
				</pre></li></div>
				<div class="col-md-6"><li><strong>Ejemplo JSON:</strong><pre>
{
"tema_1":{
          "id":"01",
          "desc_tema_1":"Economia",
          "tema_2":{
                    "id":"01",
                    "desc_tema_2":"Finanzas publicas",
                    "tema_3":{
                              "id":"01",
                              "desc_tema_3":"Finanzas publicas"
                              }
                    }
          }
}
				</pre></li></div>
				</div>
				
								<li><strong>M&eacute;todo PUT : </strong></li><div class="row"><div class="col-md-6"><strong>Body XML: </strong>
				<pre>
				
&lt;tema_3&gt;
    &lt;des_tema_3&gt;Datos&lt;/des_tema_3&gt;
&lt;/tema_3&gt;
				
				
				</pre></div><div class="col-md-6"><strong>Body JSON: </strong>
				<pre>
{
"tema_3":{
           "des_tema_3": "Datos"
           }
}
				
				</pre> </div></div>	
				
				<li><strong>Errores y Mensajes GET:</strong></li> 
				C&oacute;digo de estado HTTP 400:  La <strong>cve_entidad</strong> no cumple con el formato v&aacute;lido.<br>
				C&oacute;digo de estado HTTP 400:  La <strong>cve_municipio</strong> no cumple con el formato v&aacute;lido.<br>			
				C&oacute;digo de estado HTTP 400:  La <strong>cve_tema</strong> no cumple con el formato v&aacute;lido.<br>				
				C&oacute;digo de estado HTTP 404: Recurso no encontrado.<br>
				<li><strong>Errores y Mensajes PUT:</strong></li> 
				C&oacute;digo de estado HTTP 201: El tema  se actualizo correctamente.<br>
				C&oacute;digo de estado HTTP 400: El formato XML es incorrecto.<br>
				C&oacute;digo de estado HTTP 400: El formato JSON es incorrecto.<br>
				<li><strong>Errores y Mensajes DELETE:</strong></li> 
				C&oacute;digo de estado HTTP 200: EL dato se borro correctamente.<br>				
				

			</ul>
		</div>
		<hr>
		<div>
      		<h2>Indicadores</h2>
			<ul>
			     <li><strong>Endpoint/URL:</strong> <a href="../api/entidades/00/municipios/000/temas/01/indicadores">http://<?php echo $route?>/entidades/cve_entidad/municipios/cve_municipio/temas/cve_tema/indicadores</a></li>
				<li><strong>Descripci&oacute;n:</strong> Regresa todos los indicadores de un tema</li>
				<li><strong>M&eacute;todos HTTP:</strong> GET, POST, PUT, DELETE.</li>
			 	<li><strong>Presentaci&oacute;n de resultados:</strong> XML y JSON.</li>
				<li><strong>Par&aacute;metros:</strong> <strong>cve_tema</strong>, <strong>cve_entidad</strong>, <strong>cve_municipio</strong>.</li>
				<li><strong>Headers: </strong> <br>Accept: aplication/xml <br>
		        Accept: aplication/json
		        </li>
				<div class="row">
				<div class="col-md-6">
				<li><strong>Ejemplo XML:</strong>
				<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
    &lt;indicadores&gt;
	   &lt;indicador id="1011000028"&gt;
		  &lt;desc_indicador&gt;Disponibilidad final&lt;/desc_indicador&gt;
	   &lt;/indicador&gt;
	   .
	   .
	   .
    &lt;/indicadores&gt;
    
    
    
				</pre>
				</li>
				</div>
				<div class="col-md-6">
				<li><strong>Ejemplo JSON:</strong> 
				<pre>
{
"indicadores":[
              {
              "indicador":{
                           "id":"1011000028",
                           "desc_indicador":"Disponibilidad final"
                           }
               .
               .
               .
               }
}
				</pre>
				</li></div></div>
				
				
				<li><strong>M&eacute;todo POST : </strong></li><div class="row"><div class="col-md-6"><strong>Body XML: </strong>
				<pre>
				
&lt;indicador&gt;
    &lt;des_indicador&gt;Datos&lt;/des_indicador&gt;
&lt;/indicador&gt;
				
				
				</pre></div><div class="col-md-6"><strong>Body JSON: </strong>
				<pre>
{
"indicador":{
           "des_indicador": "Datos"
           }
}
				
				</pre> </div></div>	
				
				<li><strong>Errores y Mensajes GET:</strong></li> 
				C&oacute;digo de estado HTTP 400:  La <strong>cve_entidad</strong> no cumple con el formato v&aacute;lido.<br>
				C&oacute;digo de estado HTTP 400:  La <strong>cve_municipio</strong> no cumple con el formato v&aacute;lido.<br>		
				C&oacute;digo de estado HTTP 400: Solicitud incorrecta. Mensajes La <strong>cve_tema</strong> no cumple con el formato v&aacute;lido.<br>				
				C&oacute;digo de estado HTTP 404: Recurso no encontrado.<br>
				<li><strong>Errores y Mensajes POST:</strong></li> 
				C&oacute;digo de estado HTTP 201: El indicador se agrego correctamente con cve_indicador="ID".<br>
				C&oacute;digo de estado HTTP 400: El formato XML es incorrecto.<br>
				C&oacute;digo de estado HTTP 400: El formato JSON es incorrecto.<br>
				<li><strong>Errores y Mensajes DELETE:</strong></li> 
				C&oacute;digo de estado HTTP 200: EL dato se borro correctamente.<br>				
			
			</ul>
		</div>
		<hr>		
		<div>
      		<h2>Indicador en espec&iacute;fico</h2>
			<ul>
			<li><strong>Endpoint/URL:</strong> <a href="../api/entidades/00/municipios/000/temas/01/indicadores/1011000015">
						http://<?php echo $route?>/entidades/cve_entidad/municipios/cve_municipio/temas/cve_tema/indicadores/cve_indicador</a></li>
				<li><strong>Descripci&oacute;n:</strong> Regresa un indicador en espec&iacute;fico</li>
				<li><strong>M&eacute;todos HTTP:</strong> GET, PUT, DELETE.</li>
			 	<li><strong>Presentaci&oacute;n de resultados:</strong> XML y JSON.</li>
				<li><strong>Par&aacute;metros:</strong> <strong>cve_indicador</strong>, <strong>cve_tema</strong>, <strong>cve_entidad</strong>, <strong>cve_municipio</strong>.</li>
				<li><strong>Headers: </strong> <br>Accept: aplication/xml <br>
		        Accept: aplication/json
		        </li>
				<div class="row">
				<div class="col-md-6">
				<li><strong>Ejemplo XML:</strong> 
				<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
	&lt;indicador id="1011000015"&gt;
		&lt;desc_indicador&gt;Ingresos brutos de los municipios&lt;/desc_indicador&gt;
		&lt;anios&gt;
			&lt;periodo anio="1994"&gt;
				&lt;valor&gt;18003304&lt;/valor&gt;
			&lt;/periodo&gt;
			.
			.
			.
		&lt;anios&gt;
	&lt;indicador&gt;
	
	
	
				</pre>
				</li>
				</div>
				<div class="col-md-6">
				<li><strong>Ejemplo JSON:</strong> 
				<pre>
{
"indicador":{
             "id":"1011000015",
             "desc_indicador":"Ingresos brutos de los municipios",
             "anios":[
                     {"periodo":{
                                 "anio":"1994",
                                 "valor":"18003304"
                                 }
                     },
                     .
                     .
                     .
                     ]
             }
}</pre>
				</li></div></div>
				
					
				<li><strong>M&eacute;todo POST : </strong></li><div class="row"><div class="col-md-6"><strong>Body XML: </strong>
				<pre>
				
&lt;indicador&gt;
    &lt;des_indicador&gt;Datos&lt;/des_indicador&gt;
&lt;/indicador&gt;
				
				
				</pre></div><div class="col-md-6"><strong>Body JSON: </strong>
				<pre>
{
"indicador":{
           "des_indicador": "Datos"
           }
}
				
				</pre> </div></div>	
				
				<li><strong>Errores y Mensajes GET:</strong></li> 
				C&oacute;digo de estado HTTP 400:  La <strong>cve_entidad</strong> no cumple con el formato v&aacute;lido.<br>
				C&oacute;digo de estado HTTP 400:  La <strong>cve_municipio</strong> no cumple con el formato v&aacute;lido.<br>		
				C&oacute;digo de estado HTTP 400: Solicitud incorrecta. Mensajes La <strong>cve_tema</strong> no cumple con el formato v&aacute;lido.<br>
				C&oacute;digo de estado HTTP 400: Solicitud incorrecta. Mensajes La <strong>cve_indicador</strong> no cumple con el formato v&aacute;lido.<br>				
				C&oacute;digo de estado HTTP 404: Recurso no encontrado.<br>
				<li><strong>Errores y Mensajes POST:</strong></li> 
				C&oacute;digo de estado HTTP 201: El indicador se actulizo correctamente.<br>
				C&oacute;digo de estado HTTP 400: El formato XML es incorrecto.<br>
				C&oacute;digo de estado HTTP 400: El formato JSON es incorrecto.<br>
				<li><strong>Errores y Mensajes DELETE:</strong></li> 
				C&oacute;digo de estado HTTP 200: EL dato se borro correctamente.<br>							
				
			</ul>
		</div>
		<hr>
		<div>
      		<h2>Indicador con un año en espec&iacute;fico</h2>
			<ul>
			 <li><strong>Endpoint/URL:</strong> <a href="../api/entidades/00/municipios/000/temas/01/indicadores/1011000015/?periodo=1994">
						http://<?php echo $route?>/entidades/cve_entidad/municipios/cve_municipio/temas/cve_tema/indicadores/cve_indicador/?perido=año</a></li>
				<li><strong>Descripci&oacute;n:</strong> Regresa todos los años de un indicador</li>
				<li><strong>M&eacute;todos HTTP:</strong> GET, DELETE.</li>
			 	<li><strong>Presentaci&oacute;n de resultados:</strong> XML y JSON.</li>
				<li><strong>Par&aacute;metros:</strong> <strong>cve_indicador</strong>, <strong>cve_tema</strong>, <strong>cve_entidad</strong>, <strong>cve_municipio</strong>.</li>
				<li><strong>Headers: </strong> <br>Accept: aplication/xml <br>
		        Accept: aplication/json
		        </li>
				<div class="row">
				<div class="col-md-6">
				<li><strong>Ejemplo XML:</strong> 
				<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
    &lt;indicador id="1011000015"&gt;
        &lt;desc_indicador&gt;Ingresos brutos de los municipios&lt;/desc_indicador&gt;
        &lt;periodo anio="1994"&gt;
            &lt;valor&gt;18003304&lt;/valor&gt;
        &lt;/periodo&gt;
    &lt;/indicador&gt;
    
    
    </pre>
				</li>
				</div>
				<div class="col-md-6">
				<li><strong>Ejemplo JSON:</strong> 
				<pre>
{
"indicador":{
	"id":"1011000015",
	"desc_indicador":"Ingresos brutos de los municipios",
	"periodo":{
		  "anio":"1994",
		  "valor":"18003304"
		}
	}
}</pre>
				</li></div></div>
				<li><strong>Errores y Mensajes:</strong> </li>
				C&oacute;digo de estado HTTP 400: La <strong>cve_entidad</strong> no cumple con el formato v&aacute;lido.<br>
				C&oacute;digo de estado HTTP 400: Mensajes La <strong>cve_municipio</strong> no cumple con el formato v&aacute;lido.<br>
				C&oacute;digo de estado HTTP 400: Mensajes La <strong>cve_tema</strong> no cumple con el formato v&aacute;lido.<br>
				C&oacute;digo de estado HTTP 400: Mensajes La <strong>cve_indicador</strong> no cumple con el formato v&aacute;lido.<br>
				C&oacute;digo de estado HTTP 400: La <strong>periodo</strong> no cumple con el formato v&aacute;lido.<br>
				<li><strong>Errores y Mensajes DELETE:</strong></li> 
				C&oacute;digo de estado HTTP 200: EL dato se borro correctamente.<br>		
			</ul>
		</div>	
   </div>
</div> 


  <script src="../js/vendor/jquery-1.11.0.min.js"></script>
  <script src="../js/vendor/bootstrap.min.js"></script>
  <script src="../js/main.js"></script>
  </body>
  
  
  
</html>
